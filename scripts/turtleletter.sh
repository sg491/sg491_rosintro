#!/usr/bin/bash

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 1.57]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5, 0, 0]' '[0, 0, 4.61]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5, 0, 0]' '[0, 0, -4.61]'
