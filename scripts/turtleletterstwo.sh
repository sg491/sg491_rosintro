#!/usr/bin/bash

rosservice call /reset
rosservice call /spawn 6.5 6.4 4.7124 "turtle2"
rosservice call /turtle1/set_pen 255 0 0 3 0
rosservice call /turtle2/set_pen 0 255 0 3 0

# Draw S
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 1.57]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5, 0, 0]' '[0, 0, 4.71]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5, 0, 0]' '[0, 0, -4.71]'

# Draw h
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[4, 0, 0]' '[0, 0, 0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-1.75, 0, 0]' '[0, 0, 0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, 2.3562]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0, 0, -1.5708]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, -0.7854]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.75, 0, 0]' '[0, 0, 0]'
