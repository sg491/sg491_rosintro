#!/usr/bin/env python3

from __future__ import print_function

import sys
import copy
import rospy
import moveit_commander
import numpy as np
from moveit_msgs.msg import DisplayTrajectory
from math import pi


class Initial:

    NODE_NAME = "initial_drawer"
    GROUP_NAME = "manipulator"
    RVIZ_DISPLAY_TOPIC = "/move_group/display_planned_path"
    SCALE = 0.1

    def __init__(self):
        # Initialize nodes
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node(self.NODE_NAME)

        # Instantiate MoveIt objects
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.move_group = moveit_commander.MoveGroupCommander(self.GROUP_NAME)

        # Initialize rviz publisher
        self.dt_pub = rospy.Publisher(
            self.RVIZ_DISPLAY_TOPIC, DisplayTrajectory, queue_size=20)

    def _get_info(self):
        """Display information about the robot"""
        planning_frame = self.move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        eef_link = self.move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        group_names = self.robot.get_group_names()
        print("============ Available Planning Groups:", group_names)

        print("============ Printing robot state")
        print(self.robot.get_current_state())

    def run(self):
        """Execute a motion plan to draw the initial S using MoveIt"""
        self._get_info()
        self._move_to_start()
        self._draw_initial()
        self._get_info()

    def _move_to_start(self):
        """Moves the arm to the starting configuration to avoid singularities"""
        joint_goal = self.move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = -pi / 4
        joint_goal[2] = pi / 4
        joint_goal[3] = 0
        joint_goal[4] = pi / 2
        joint_goal[5] = 0

        self.move_group.go(joint_goal, wait=True)
        self.move_group.stop()

    def _draw_initial(self):
        """Draws an initial using UR5 arm"""
        plan, fraction = self._create_initial_plan()
        self._display_trajectory(plan)
        self.move_group.execute(plan, wait=True)

    def _display_trajectory(self, plan):
        dt = DisplayTrajectory()
        dt.trajectory_start = self.robot.get_current_state()
        dt.trajectory.append(plan)
        self.dt_pub.publish(dt)

    def _create_initial_plan(self):
        """Create waypoints that describe movement plan that draws an initial"""
        waypoints = []
        wpose = self.move_group.get_current_pose().pose

        # Split letter "S" into 6 curve segments
        self._add_curve_segment(-1, 1, wpose, waypoints)
        self._add_curve_segment(-1, -1, wpose, waypoints)
        self._add_curve_segment(1, -1, wpose, waypoints)
        self._add_curve_segment(1, -1, wpose, waypoints)
        self._add_curve_segment(-1, -1, wpose, waypoints)
        self._add_curve_segment(-1, 1, wpose, waypoints)

        print(waypoints)

        # Create motion plan
        (plan, fraction) = self.move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )

        return plan, fraction
    
    def _add_curve_segment(self, y, z, wpose, waypoints):
        """Adds a curve segment that moves to the given y and z coordinates"""
        wpose.position.y += self.SCALE * y
        wpose.position.z += self.SCALE * z
        waypoints.append(copy.deepcopy(wpose))  # Add final waypoint


def main():
    try:
        Initial().run()  # Run initial script
    except rospy.ROSInterruptException:
        pass


if __name__ == "__main__":
    main()
