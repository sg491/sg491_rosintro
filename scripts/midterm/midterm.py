#!/usr/bin/env python3

from __future__ import print_function

import sys
import copy
import rospy
import moveit_commander
import numpy as np
from moveit_msgs.msg import DisplayTrajectory
from math import pi


class Initials:
    """Draws the Initials SOG. I don't have a middle name, so I chose `O`."""
    NODE_NAME = "initials_drawer"
    GROUP_NAME = "manipulator"
    RVIZ_DISPLAY_TOPIC = "/move_group/display_planned_path"
    SCALE = 0.1

    def __init__(self):
        # Initialize ROS nodes
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node(self.NODE_NAME)

        # Instantiate MoveIt objects
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.move_group = moveit_commander.MoveGroupCommander(self.GROUP_NAME)

        # Initialize rviz publisher
        self.dt_pub = rospy.Publisher(
            self.RVIZ_DISPLAY_TOPIC, DisplayTrajectory, queue_size=20)

    def _get_info(self):
        """Display information about the robot for debugging"""
        planning_frame = self.move_group.get_planning_frame()
        rospy.loginfo(f"============ Planning frame: {planning_frame}")

        eef_link = self.move_group.get_end_effector_link()
        rospy.loginfo(f"============ End effector link: {eef_link}")

        group_names = self.robot.get_group_names()
        rospy.loginfo(f"============ Available Planning Groups: {group_names}")

        rospy.loginfo("============ Printing robot state")
        rospy.loginfo(self.robot.get_current_state())

    def run(self):
        """Execute a motion plan to draw the letters SOG using MoveIt"""
        # Move to starting config
        self._move_to_start()
        # Draw initials SOG, moving to starting config between letters
        self._draw_s()
        self._move_to_start()
        self._draw_o()
        self._move_to_start()
        self._draw_g()
        # Display info for debugging
        self._get_info()

    def _move_to_start(self):
        """Moves the arm to a starting configuration, avoids singularities"""
        joint_goal = self.move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = -pi / 2
        joint_goal[2] = pi / 2
        joint_goal[3] = 0
        joint_goal[4] = pi / 2
        joint_goal[5] = 0

        self.move_group.go(joint_goal, wait=True)
        self.move_group.stop()

    def _execute_motion_plan(self, waypoints):
        """Creates and executes a motion plan from a set of waypoints"""
        plan, fraction = self.move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )
        self._display_trajectory(plan)
        self.move_group.execute(plan, wait=True)

    def _draw_s(self):
        """Draws an `S` using UR5 arm"""
        waypoints = []
        wpose = self.move_group.get_current_pose().pose
        # Move arm left to start drawing S
        wpose.position.y -= self.SCALE * 2
        waypoints.append(copy.deepcopy(wpose))
        # Split letter "S" into 2 circular curve segments
        self._add_circular_curve(wpose, waypoints, -1*self.SCALE, 0, 0, 3*pi/2)
        self._add_circular_curve(wpose, waypoints, 0, -1*self.SCALE, pi/2, -pi)
        # Execute motion plan
        self._execute_motion_plan(waypoints)

    def _draw_o(self):
        """Draws an `O` using UR5 arm"""
        waypoints = []
        wpose = self.move_group.get_current_pose().pose
        # Move arm up to start drawing O
        wpose.position.z += self.SCALE * 1
        waypoints.append(copy.deepcopy(wpose))
        # Split letter "O" into 2 circular curve segments
        self._add_circular_curve(
            wpose, waypoints, 0, -2*self.SCALE, pi/2, 5*pi/2)
        # Execute motion plan
        self._execute_motion_plan(waypoints)

    def _draw_g(self):
        """Draws a `G` using UR5 arm"""
        waypoints = []
        wpose = self.move_group.get_current_pose().pose
        # Move arm right to start drawing G
        wpose.position.y += self.SCALE * 2
        waypoints.append(copy.deepcopy(wpose))

        # Split letter "G" into circular curve and straight lines
        self._add_circular_curve(wpose, waypoints, -np.sqrt(2)*self.SCALE,
                                 -np.sqrt(2)*self.SCALE, pi/4, 7*pi/4)
        # Upward straight line
        wpose.position.z += np.sqrt(2)*self.SCALE
        waypoints.append(copy.deepcopy(wpose))
        # Sideways straight line
        wpose.position.y -= self.SCALE
        waypoints.append(copy.deepcopy(wpose))

        # Execute motion plan
        self._execute_motion_plan(waypoints)

    def _display_trajectory(self, plan):
        """Displays trajectories in RVIZ"""
        dt = DisplayTrajectory()
        dt.trajectory_start = self.robot.get_current_state()
        dt.trajectory.append(plan)
        self.dt_pub.publish(dt)

    def _add_circular_curve(self, wpose, waypoints, rcy, rcz, theta_start,
                            theta_stop, dt=pi/8):
        """Adds a set of waypoints representing a circular curve.

        Args:
            wpose: A Pose with starting location. Stores final waypoint
            waypoints: A list of waypoints that the curve will be appended to
            rcy: The y center of the circle, RELATIVE to starting position
            rcz: The z center of the circle, RELATIVE to starting position
            theta_start: The starting angle of the curve
            theta_stop: The final angle of the curve
            dt: The theta step to use to determine waypoint granularity
        """
        start = copy.deepcopy(wpose)
        # Calculate radius of circle
        r = np.sqrt((rcy)**2 + (rcz)**2)
        # Want to move clockwise
        if theta_start > theta_stop:
            dt *= -1
        # Loop through angles, adding points to curve
        for theta in np.arange(theta_start + dt, theta_stop + dt, dt):
            # Parametric equation for a circle
            wpose.position.y = (start.position.y + rcy) + r * np.cos(theta)
            wpose.position.z = (start.position.z + rcz) + r * np.sin(theta)
            waypoints.append(copy.deepcopy(wpose))


def main():
    try:
        Initials().run()  # Run initial script
    except rospy.ROSInterruptException:
        pass


if __name__ == "__main__":
    main()
